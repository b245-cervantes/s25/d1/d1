console.log("Hello JSON!");

//[Section] JSON Objects
	//JSON stands for JavaScript Object Notation
	//JSON is also used in other programming languages
	//core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into Javascript Objects
		//JSON Object- it means parsed
		//Stringified JSON Object - JSON Objecty na nakastringify
	//JSON is used for serializing/converting different data types
	/*
		Syntax/Format:
		{
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		}
	*/

	//JSON Object
	/*{
		"city" : "Quezon City",
		"province": "Manila",
		"country" : "Philippines"
	}*/

	//JSON Arrays
		//array of JSON Objects

	/*[
		{
			"city" : "Quezon City",
			"province": "Manila",
			"country" : "Philippines"
		},
		{
			"city" : "Manila City",
			"province" : "Manila",
			"country" : "Philippines"
		}

		]*/

	//[Section] JSON Methods
		//JSON methods contains methods for parsing and converting data into stringified JSON

	let batchesArr = [
			{
				batchName: "Batch X"
			},
			{
				batchName: "Batch Y"
			}
		];

	console.log("This is the original array:");
	console.log(batchesArr);
		//The stringify method is used to convert JavaScript Objects/Arrays into a string.
	let stringBatchesArr = JSON.stringify(batchesArr);

	console.log("This is the result of the stringify method:");
	console.log(stringBatchesArr);
	console.log("Data type:")
	//To check the data type of the array after the stringify method
	console.log(typeof stringBatchesArr);

	console.log("This is the original array of objects:")
	console.log(batchesArr);

	let data = JSON.stringify({
		name: 'John',
		address: {
			city: "Manila",
			country: "Philippines"
		}
	})

	console.log(data);

	//[Section] Use stringify method with variables
		//When information is stored in a variable and is not hard coded into an object tha is being stringified, we can supply the value with a varibable;

	/*let firstName = prompt("What is your first name?");
	let lastName = prompt("What is your last name?");
	let age = Number(prompt("How young are you?"));
	let address = {
		city: prompt("What city do you live in?"),
		country: prompt("Which country does your city address belong?")
	}

	let otherData = JSON.stringify({
		firstName,
		lastName,
		age,
		address
	})

	console.log(otherData);*/

	//[Section] Converting stringified JSON into JavaScript Objects
		/*
			-objects are common data types used in application because of the complex data structures that can be created out of them.
			-information is commonly sent to application in stringified JSON and then converted back into objects
			-This happes both for sending infromation to a backend applciation and sending information back to frontend application

		*/
	//parse method converts the stringify JSON into JSON object;
	let objectBatchesArr = JSON.parse(stringBatchesArr);
	console.log("This is the stringify version:");
	console.log(stringBatchesArr);

	console.log("This is the result after the Parse method:")
	console.log(objectBatchesArr);
	console.log(typeof objectBatchesArr);

	console.log(objectBatchesArr[0])

	let stringifiedObject = `{
		"name": "John",
		"age": 31,
		"address": {
			"city" : "Manila City",
			"country" : "Philippines"
		}
	}`;

	console.log(JSON.parse(stringifiedObject));

